![Text banner that says "Desiderium" in uppercase, and "Theme 03 by glenthemes" at the bottom](https://64.media.tumblr.com/4c11ed9352cf4470ad8a02fc6c0b3daa/e59a013f18682b44-cc/s1280x1920/2e36ceaa523539bca03b06f257b74c1204010698.png)  
![Screenshot preview of the theme "Desiderium" by glenthemes](https://64.media.tumblr.com/519991564454e915d6853ad4e21e1b12/e59a013f18682b44-c2/s1280x1920/62abb0a45b918d63d90e1d271b7cc7dc68684096.png)

**Theme no.:** 03  
**Theme name:** Desiderium  
**Theme type:** Free / Tumblr use  
**Description:** ♰ Dēsīderium, (n.) an ardent desire or longing; <small>GRIEF OR REGRET FOR THE ABSENCE OR LOSS OF SOMEONE ONCE HAD</small>. Previously titled 《 Fools 》, this theme now features Jack and Oswald from Pandora Hearts, inspired by victorian elements and dark academia, with art by [@surfacage](https://surfacage.tumblr.com/post/54339673119/like-time-suspended-a-wound-unmended).  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-08-11](https://64.media.tumblr.com/a95fdbb7952ccc99a0eb7dfb3c247d64/tumblr_nswxuc15Zw1ubolzro1_1280.png)  
**Rework date:** [2016-11-08](https://64.media.tumblr.com/4d497be389483101b9577748f7a2e2a1/tumblr_nswxuc15Zw1ubolzro2_r1_1280.png)

**Post:** [glenthemes.tumblr.com/post/662365475840442368](https://glenthemes.tumblr.com/post/662365475840442368)  
**Preview:** [glenthpvs.tumblr.com/desiderium](https://glenthpvs.tumblr.com/desiderium)  
**Download:** [pastebin.com/JpxDfvTS](https://pastebin.com/JpxDfvTS)  
**Credits:** [glencredits.tumblr.com/desiderium](https://glencredits.tumblr.com/desiderium)
